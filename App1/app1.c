#include <stdio.h>
#include "part1.h"

void f2(void);          // Defined in this file (at the bottom)

int main(void) {

    f1();               // Defined in part1.c
    f2();

    return(0);
}

void f2(void) {
  printf("This is f2\n");
}