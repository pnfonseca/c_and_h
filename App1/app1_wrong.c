#include <stdio.h>
#include "part1.c" // "xx", not <xx>

void f2(void);

int main(void) {
    f1();          // defined in part1.c
    f2();
    return 0;
}

void f2(void) {
    printf("This is f2\n");
}