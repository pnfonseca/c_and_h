#include <stdio.h>

/* Declares var1 as an extern variable
   so that calling code can refer to var1 */
extern int var1;

/* Declares the propotype of print_value */
void print_value(void);
